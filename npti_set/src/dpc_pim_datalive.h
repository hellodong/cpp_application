
#ifndef _DPC_PIM_DATALIVE_H_
#define _DPC_PIM_DATALIVE_H_

#include <stdint.h>
#include <iostream>
#include <mutex>
#include <set>

#ifdef __cplusplus
extern "C"
{
#endif

struct dp_ip_key_t{
    uint16_t iptype;
    uint16_t mask;
    uint8_t ip[16];
};



#ifdef __cplusplus
}
#endif

struct dp_cm_pim_mc_group_key_t
{
    dp_cm_pim_mc_group_key_t():inlif(0) 
    {
        source.iptype = 0;
        source.mask = 0;
        source.ip[0] = 0;
        group.iptype = 0;
        group.mask = 0;
        group.ip[0] = 0;
    }

    dp_cm_pim_mc_group_key_t(uint32_t _inlif, dp_ip_key_t _source, dp_ip_key_t _group):inlif(_inlif) 
    {
        source.iptype = _source.iptype;
        source.mask = _source.mask;
        group.iptype = _group.iptype;
        group.mask = _group.mask;
        for (int idx = 0;idx < 16;idx++)
        {
            source.ip[idx] = _source.ip[idx];
            group.ip[idx] = _source.ip[idx];
        }
    }

    bool operator<(const dp_cm_pim_mc_group_key_t &rhs) const
    {
        if(inlif < rhs.inlif)
        {
            return true;
        }
        else if(inlif > rhs.inlif)
        {
            return false;
        }
        int ip_len = rhs.source.iptype == 1? 4:16;

        for (int idx = 0; idx < ip_len;idx++)
        {
            auto ret = source.ip[idx] == rhs.source.ip[idx];
            if (ret)
            {
                continue;
            }
            if (source.ip[idx] < rhs.source.ip[idx])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        for(int idx = 0;idx < ip_len;idx++)
        {
               bool ret =  (group.ip[idx] == rhs.group.ip[idx]);
               if (ret)
               {
                    continue;
               }

               if (group.ip[idx] > rhs.group.ip[idx])
               {
                   return true;
               }
               else
               {
                   return false;
               }
         }
         return false;
    }

    uint32_t inlif; 
    dp_ip_key_t source;
    dp_ip_key_t group;
};


class Dpc_pim_dataliveness {
    public:
       Dpc_pim_dataliveness() = default;
       int insert(uint32_t inlif, dp_ip_key_t &source, dp_ip_key_t &group);
       void erase(uint32_t inlif, dp_ip_key_t &source, dp_ip_key_t &group);
       void flush(uint32_t is_ipv4);
       size_t dataliveness_size(uint32_t is_ipv4);
       size_t dump(void);
    private:
       std::mutex mutex_;
       std::set<dp_cm_pim_mc_group_key_t> ipv4_set;
       std::set<dp_cm_pim_mc_group_key_t> ipv6_set;
};

extern "C"
{

size_t dpc_pim_cache_datalivness_size(uint32_t is_ipv4);
int dpc_pim_cache_dataliveness_add(uint32_t inlif, dp_ip_key_t *source, dp_ip_key_t *group);


}


#endif //dpc_pim_datalive.h

