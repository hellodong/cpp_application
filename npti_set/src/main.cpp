

#include "dpc_pim_datalive.h"


int main(int argc, char *argv[])
{
    dp_ip_key_t source = {1, 32, {1,1,1,1}}, group = {1,32, {225,0,0,1}};

    std::cout << dpc_pim_cache_datalivness_size(1) <<std::endl;
    dpc_pim_cache_dataliveness_add(1, &source, &group);
    std::cout << dpc_pim_cache_datalivness_size(1) <<std::endl;
    return 0;
}
