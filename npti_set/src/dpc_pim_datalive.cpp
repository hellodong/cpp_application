

#include "dpc_pim_datalive.h"


static Dpc_pim_dataliveness *g_pim_dataliveness = nullptr;

int Dpc_pim_dataliveness::insert(uint32_t inlif, dp_ip_key_t &source, dp_ip_key_t &group)
{
    std::lock_guard<std::mutex> lock(mutex_);
    dp_cm_pim_mc_group_key_t _entry{inlif, source, group};
    int rt = 0;
    if (source.iptype == 1)
    {
        auto ret = ipv4_set.insert(_entry);
        rt = static_cast<int>(ret.second);
    }
    else 
    {
        auto ret = ipv6_set.insert(_entry);
        rt = static_cast<int>(ret.second);
    }

    return rt;
}

void Dpc_pim_dataliveness::erase(uint32_t inlif, dp_ip_key_t &source, dp_ip_key_t &group)
{
    dp_cm_pim_mc_group_key_t _entry{inlif, source, group};
    std::lock_guard<std::mutex> lock(mutex_);

    if (source.iptype == 1)
    {
        ipv4_set.erase(_entry);
    }
    else 
    {
        ipv6_set.erase(_entry);
    }
    return;
}

size_t Dpc_pim_dataliveness::dataliveness_size(uint32_t is_ipv4)
{
    std::lock_guard<std::mutex> lock(mutex_);
    return is_ipv4 ? ipv4_set.size() : ipv6_set.size();
}

void Dpc_pim_dataliveness::flush(uint32_t is_ipv4)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if(is_ipv4)
    {
        ipv4_set.clear();
    }
    else
    {
        ipv6_set.clear();
    }
}

extern "C" 
{
    Dpc_pim_dataliveness *__dpc_pim_dataliveness_get_inst(void)
    {
        if (nullptr == g_pim_dataliveness)
        {
            g_pim_dataliveness = new Dpc_pim_dataliveness;
        }
        return g_pim_dataliveness;
    }

    size_t dpc_pim_cache_datalivness_size(uint32_t is_ipv4)
    {
        return __dpc_pim_dataliveness_get_inst()->dataliveness_size(is_ipv4);
    }
    
    int dpc_pim_cache_dataliveness_add(uint32_t inlif, dp_ip_key_t *source, dp_ip_key_t *group)
    {
        return __dpc_pim_dataliveness_get_inst()->insert(inlif, *source, *group);
    }

    void dpc_pim_cache_dataliveness_delete(uint32_t inlif, dp_ip_key_t *source, dp_ip_key_t *group)
    {
        return __dpc_pim_dataliveness_get_inst()->erase(inlif, *source, *group);
    }
}

